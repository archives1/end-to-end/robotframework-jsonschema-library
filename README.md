# Robotframework with Json Schema Library

## Install Json Schema Library

```bash

pip3 install -r requirements.txt

```


## Run Robot at test.robot

```bash

robot test.robot

```

