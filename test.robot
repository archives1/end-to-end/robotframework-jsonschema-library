*** Settings ***
Library    JSONSchemaLibrary    ./



*** Test Cases ***
My Test Case:
    Check Json Schema


*** Keywords ***
Check Json Schema
    ${data}    Create Dictionary    name=Eggs    price=${34.99}
    Validate Json   schema_name.schema.json  ${data}
    ${my_list}    Create List    ${data}    ${data}
    Validate Json   schema_name_in_list.schema.json  ${my_list}
